#include <iostream>
#include <algorithm>
using namespace std;


int main(){

	int num;
	cin>>num;

	int r,g,b;
	int tr=0, tg=0, tb=0;

	for(int i=0;i<num;i++){
		int ir=tr, ig=tg, ib=tb;
		cin>>r>>g>>b;

		tr=r+min(ig,ib);
		tg=g+min(ir,ib);
		tb=b+min(ir,ig);
	}
	
	cout<<min(tr,min(tg,tb))<<endl;
	return 0;
}