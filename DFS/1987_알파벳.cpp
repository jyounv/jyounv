#include <iostream>
using namespace std;


// 1987_알파벳
char map[20][20]; // 1-20

int check[20][20];
char check_alp[26];
int cnt,MAX;
int dx[4]={1,0,-1,0};
int dy[4]={0,1,0,-1};
int r,c; //세로 r 가로 c  map[r][c];

void dfs(int x, int y,int cnt,int check[20][20],char check_alp[26]){
	check[x][y]=1;
	check_alp[map[x][y]-'A']=1;
	
	
		for(int i=0;i<4;i++){
			int nx=x+dx[i];
			int ny=y+dy[i];
			
			if(nx<r && ny<c && nx>=0 && ny>=0) {
				if(check[nx][ny]!=1 && check_alp[map[nx][ny]-'A']!=1) {
					dfs(nx,ny,cnt+1,check,check_alp);
				}
			}
		}
		if(cnt>MAX) {
			MAX = cnt; 
		}

}

int main(){
	freopen("input.txt", "r", stdin);
	cin>> r >> c;
	for(int i=0;i<r;i++){
		for(int j=0; j<c;j++){
			cin>>map[i][j];
		}
	}
	dfs(0,0,1,check,check_alp); 
	cout<< MAX <<endl;
	return 0;
}