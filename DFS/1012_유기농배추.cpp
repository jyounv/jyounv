#include <iostream>
#include <cstring>
using namespace std;


//2017-08-14 1012_유기농 배추(DFS)

int n,m;//세로x가로
int k;// 배추갯수 
int map[50][50];
int check[50][50];
int bug;
int t;
int dx[4]={1,0,-1,0};
int dy[4]={0,1,0,-1};

void dfs(int x, int y){

	check[x][y]=1; 
	for(int i=0;i<4;i++){
		int nx=x+dx[i];
		int ny=y+dy[i];
		if(nx<n && ny<m && nx>=0 && ny>=0) {
			if(check[nx][ny]!=1 && map[nx][ny]==1) {
				dfs(nx,ny);
			}
		}
		//if(map[nx][ny]==1&&check[nx][ny]!=1&& nx<n && ny<m&&nx>=0&&ny>=0)		
	}
}	

int main(){

	freopen("input.txt", "r", stdin);
	cin>>t;	
	for(int T=0; T<t;T++){
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				map[i][j]=0;
				check[i][j]=0;
			}
		}
		bug=0;
		cin>>m>>n>>k;
		int x,y;
		for(int i=0;i<k;i++){
			cin>>y>>x;
			map[x][y]=1;
		}
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				if(map[i][j]==1&&check[i][j]!=1){
						dfs(i,j);
						bug++;
				}
			}
		}
		cout << bug<<endl;
	}
	return 0;
}